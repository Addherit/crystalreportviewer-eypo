﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Odbc;
using System.Data.SqlClient;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
namespace CrystalReportViewer
{
    public partial class WebForm5 : System.Web.UI.Page
    {
    
        protected void Page_Load(object sender, EventArgs e)
        {
            ReportDocument myReportDocument;
           
            myReportDocument = new ReportDocument();


            string folio;
            string empresa;
            folio = Request.QueryString["folio"];
            empresa = Request.QueryString["empresa"];
                                 
            myReportDocument.Load(@"c:\xampp1\htdocs\eypo\arcosConsultas\rpt\"+empresa+".rpt");
            myReportDocument.SetDatabaseLogon("sa", "3yP02020", "SEYPO", "dbEypo");
            //myReportDocument.SetDatabaseLogon("Desarrollo", "Inn0vaci0n", "LAPTOP-VSR73J04", "Cajero");
            myReportDocument.SetParameterValue(0, empresa);
            myReportDocument.SetParameterValue(1, folio);
                        
            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, @"c:\xampp1\htdocs\eypo\visor\tres\RA " + folio+"-"+empresa+".pdf");
            //myReportDocument.Close();


        }
    }
}